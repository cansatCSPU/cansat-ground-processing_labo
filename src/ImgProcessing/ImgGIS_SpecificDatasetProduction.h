/*
 * ImgGIS_SpecificDatasetProduction.h
 *
 *  Created on: 20 avr. 2019
 *      Author: Steven et Bryan
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
#include "RTP_ConfigDataSet.h"
#include "CameraCalibrationDataSet.h"
#include "JPEG_Image.h"

class ImgGIS_SpecificDatasetProduction : public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	ImgGIS_SpecificDatasetProduction(
			const RTP_ConfigDataSet theRTP_Config,
			const CameraCalibrationDataSet theCameraCalib,
			const std::string& theFolderIn,
			const std::string& theFolderOut);
	virtual ~ImgGIS_SpecificDatasetProduction();
protected:
	virtual void doTerminate();
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut);
	void resampler(const IsaTwoGroundRecord& record, string imageName, string imageResampledName);

	/** Generate a contour dataset with 1 layer for the particular record provided.
	 *  @param recordIn The record to generate a contour for.
	 *  @param GIS_ContourPath The absolute or relative path to the GIS_Contour dataset.
	 */
	void generateContourDS(
				const IsaTwoGroundRecord& recordIn,
				const std::string& GIS_ContourPath);

	void getColor(const unsigned int x, const unsigned int y, unsigned char *&red,unsigned char *&green, unsigned char *&blue, JPEG_Image &image);
private:
	static constexpr bool DBG=false;
	static constexpr bool DBG_ALL_PIXELS=false;
	static constexpr bool LimitResampledImageResolution=true; /**< if true, resolution is limited (at least
	 	 	 	 	 	 	 	 	 	 	 	 	one dimension must have less than 3 times the number
	 	 	 	 	 	 	 	 	 	 	 	 	of pixels in the largest dimension of the original
	 	 	 	 	 	 	 	 	 	 	 	 	image */
	RTP_ConfigDataSet RTP_Config;
	CameraCalibrationDataSet cameraCalib;
	std::string folderIn;
	std::string	imgFolderIn;	/**< path to the directory containing the image files provided by the can. */
	std::string imgFolderOut;	/**< path to the destination directory for the mapped image files. */
	unsigned int imageCounter;  /**< the number of images actually processed. */
	JPEG_Image image;
	double latDPM;
	double longDPM;
};
