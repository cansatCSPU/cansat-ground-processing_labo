/*
 * JPEG_Image.h
 */

#pragma once

#include <string>
#include <exception>

/** @ingroup ImgProcessing
 *  @brief A class providing access to every part of an image stored in JPEG format.
 *  Relies on libjpeg for the actual work accessing the file and decompressing
 *  the image
 *  Based on from https://stackoverflow.com/questions/5616216/need-help-in-reading-jpeg-file-using-libjpeg
 *  Derived from Tom Lane's example.c (Obtain & install jpeg stuff from web
 *  (jpeglib.h, jerror.h jmore.h, jconfig.h,jpeg.lib))
 */
class JPEG_Image {
public:

	JPEG_Image(const std::string &fileName);
	JPEG_Image();
	JPEG_Image(const JPEG_Image &other) = delete;
	virtual ~JPEG_Image();

	virtual void loadImage(const std::string& fileName) ;
	virtual unsigned char * getBitmap() const;
	virtual unsigned int getSizeX() const;
	virtual unsigned int getSizeY() const;
	virtual unsigned int getNumChannels() const;

	/** For test only: add 3 pure R, G, and B blocks in topleft corner. */
	virtual void addTestRGB_Blocks(unsigned int blockLength=150, unsigned int blockHeight=150);
private:
	unsigned char *bitmap; // The raw data for the image.
	unsigned int sizeX;
	unsigned int sizeY;
	unsigned int numChannels;
	// size, num channels etc etc.
};

