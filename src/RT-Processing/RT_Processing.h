/*
 * RT_Processing.h
 */

/** @defgroup Application Ground Processing Application
 *  @brief The set of classes managing the User Interface for the Ground Processing application.
 *
 *  The Ground Processing Application package contains all classes used to manage the user interface,
 *  command line options etc.
 *
 *  _Dependencies_\n
 *  This package relies on:
 *  	- the Utils package
 *  	- the easylogging++ library
 *
 *  @todo //.
 *
 *
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (IsaTwo).
 */
#pragma once

// Include cxxopts.hpp without generating warnings.
#pragma GCC diagnostic push
// The first pragma is to silence warnings about some of the
// next pragmas not being recognized by minGW.
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wshorten-64-to-32"
#pragma GCC diagnostic ignored "-Wfloat-conversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#include "cxxopts.hpp"
#pragma GCC diagnostic pop

#include "RecordSource.h"
#include "IsaTwoRecord.h"
#include "IsaTwoGroundRecord.h"
#include <IMU_CalibrationDataSet.h>
#include "CameraCalibrationDataSet.h"
#include "RTP_ConfigDataSet.h"

/** @ingroup Application
 *  @brief The RT_Processing class is the entry point for the RT_Processing executable.
 *  It is in charge of:
 *  	- Extracting information from the command-line
 *  	- Configuring the environment
 *  	- Launching the required service.
 */
class RT_Processing {
public:
	RT_Processing();
	// prevent copy and assignment
	RT_Processing(const RT_Processing&) = delete;
	RT_Processing& operator=(const RT_Processing &) = delete;

	virtual ~RT_Processing();

	/** Run the RT_Processing
	 *  @param argc The number of arguments on the command-line
	 *  @param argv The command-line arguments
	 */
	virtual int run(int argc, char* argv[]);

protected:
	/** The various possibilities for the sources providing the input records */
	enum class SourceType_t {
		file, serial, dummyStrings, dummyRecords, dummyGroundRecords
	};

	/** Actually launch processing after successful command-line parsing.
	 *  @pre Command line has been successfully parsed, and result is in
	 *       member parseResult.
	 *  @exception All cxxopts exceptions relevant to command-line
	 *             configuration and parsing, runtime_errors in case of
	 *             inconsistent command-line options.
	 */
	virtual void performProcessing();

	/** Register all valid options and parameters into the options object.
	 *  @param argc The number of arguments on the command-line
	 *  @param argv The command-line arguments
	 *  @exception All cxxopts exceptions relevant to command-line
	 *             configuration and parsing.
	 */
	virtual void processCommandLine(int argc, char* argv[]);

	/** Check existence of required files and directories, create missing ones */
	virtual void installDataFileStructure();

	/** Output help information on cout */
	virtual void printHelp() const;

	/** @brief Analyse the options defining the source of the data to process.
	 *  handles the source-type, and source-file options to set the sourceType, sourceFile
	 *  and fileBase data members as required. If source is not 'file', defineSourceFileAndFileBase()
	 *  must be called to obtain the source file and have the fileBase configured; otherwise,
	 *  fileBase is properly configured by this method.
	 *
	 *  @return The source type to use.
	 */
	virtual SourceType_t defineSourceType();

	/** @brief Define the file from which data must be processed, and configure the fileBase accordingly.
	 *  (see rules in Architecture document (1200).
	 *  @pre defineSourceType returned SourceType_t::file.
	 */
	virtual std::string defineSourceFileAndFileBase();

	/** @brief Obtain the fully validated path to a configuration file.
	 * This method is used for any kind of configuration file (IMU calibration, RTP settings, etc.
	 * The path is defined as follows:
	 * If option --cmdLineOption is provided, its value is used as relative path
	 * to data/settingsFileDir. If the file does not exist, an exception is thrown.
	 * If option --cmdLineOption is not provided, a default calibration file
	 * is used as follows.
	 * 	- If --source-type=serial, use file default file
	 * 	  "pathToRT_Processing/data/\<settingsFileDir\>/default.\<subExtension\>.\<extension\>", if any.
	 * 	- Else build a name by replacing A_Records with "<settingsFileDir>" in the fileBase,
	 * 	  and appending “.\<subExtension\>.\<extension\>” to it.
	 * 	  	Example: if fileBase = /a/b/c/data/A_Records/myFolder/myFile, considered configuration
	 * 	  	file is /a/b/c/data/\<settingsFileDir\>/myFolder/myFile.imu.csv.
	 * 	  If the file exists, use it else use same default file as above.
	 * 	 @param cmdLineOption The name of the option that can be used to manually force a
	 * 	 		particular configuration file.
	 * 	 @param defaultConfigFileName The name of the configuration file to use when
	 * 	 		there is no way to define a more specific one (e.g when data is received from
	 * 	 		the serial line) or when specific files do not exist.
	 * 	 @param settingsFileDir The path within the data/ folder, to a folder containing
	 * 	 		the considered configuration files.
	 * 	 @param subExtension The sss (any number of character) sub-extension used for considered
	 * 	        configuration files (like in generalSettings.xxx.csv). No quotes, no dots should
	 * 	        be included.
	 * 	 @param extension The xxx extension used for considered configuration files (typically
	 * 	        'csv' (no quotes, no dots).
	 * 	 @pre fileBase and srcType have been set, by calling defineSourceType() and then
	 * 	      defineSourceFileAndFileBase() if source type is "file".
	 * 	 @return The path to an existing calibration file to use.
	 */
	virtual std::string getConfigFileName(
			const std::string& cmdLineOption,
			const std::string& defaultConfigFileName,
			const std::string& settingsFileDir, const std::string& subExtension,
			const std::string& extension);

	/** @brief Obtain a fully initialized IMU_CalibrationDataSet object, ready for use
	 * in the calculations.
	 * See method #getConfigFileName() for details about the choice of the source file.
	 * 	 @pre fileBase and srcType have been set, by calling defineSourceType() and then
	 * 	      defineSourceFileAndFileBase() if source type is "file".
	 * 	 @return The ready-to-use CalibrationDataSet object.
	 */
	virtual IMU_CalibrationDataSet getIMU_CalibrationDataSet();

	/** @brief Obtain a fully initialized CameraCalibrationDataSet object, ready for use
	 * in the calculations.
	 * See method #getConfigFileName() for details about the choice of the source file.
	 * 	 @pre fileBase and srcType have been set, by calling defineSourceType() and then
	 * 	      defineSourceFileAndFileBase() if source type is "file".
	 * 	 @return The ready-to-use CameraCalibrationDataSet object.
	 */
	virtual CameraCalibrationDataSet getCameraCalibrationDataSet();

	/** @brief Obtain a fully initialized RTP_ConfigDataSet object, ready for use
	 * in the calculations.
	 * See method #getConfigFileName() for details about the choice of the source file.
	 * 	 @pre fileBase and srcType have been set, by calling defineSourceType() and then
	 * 	      defineSourceFileAndFileBase() if source type is "file".
	 * 	 @return The ready-to-use RTP_ConfigDataSet object.
	 */
	virtual RTP_ConfigDataSet getConfigurationDataSet();

	/** Create record source according to command-line options */
	virtual void configureRecordSource();

	/** Create the required processing chain according to command-line options
	 *  @pre source file options have been analyzed by calling analyseSourceAndFileOptions().
	 *  @return true if the result of the processing is unstructured strings,
	 *          false if it consists of IsaTwoGroundRecords.*/
	virtual bool configureProcessingChain();

	/** Append the required processor to the chain connected to the record source.
	 *  @param resultIsString True if the processing chain produces unstructured
	 *  					  string, false if it produces IsaTwoGroundRecords.
	 *  @pre The whole process chain is configured (from the headProcessor).
	 */
	virtual void appendResultSink(bool resultIsString);

	cxxopts::Options options; /**< command-line parser object */
	cxxopts::ParseResult* parseResult { nullptr }; /**< Pointer to the result of command-line parsing. */
	RecordSource* recordSource; /**< The record source from which data will be obtained */
	std::string fileBase; /**< The absolute path for output files, without the final extension */
	SourceType_t srcType; /**< The type of record source used (see enum values). */
};

