/*
 * IMU_CalibrationDataSet.cpp
 *
 *  Created on: 23 Nov 2018
 *      Author: Alain
 */

#include <IMU_CalibrationDataSet.h>
#include <iostream>
#include "FileMgr.h"
#include "DebugUtils.h"
#include "CSV_Row.h"

static constexpr bool DBG=false;

using namespace std;

IMU_CalibrationDataSet::IMU_CalibrationDataSet() {
	for (auto i=0UL; i<3;i++) {
		magTransformationMatrix[i].fill(0.0);
	}
	magOffset.fill(0.0);
	gyroResolution.fill(0.0);
	gyroOffset.fill(0.0);
	accelResolution.fill(0.0);
	accelOffset.fill(0.0);
}

IMU_CalibrationDataSet::IMU_CalibrationDataSet(const std::string& dataFile) {
	if (!FileMgr::isRegularFile(dataFile)) {
		throw runtime_error(string("Inexistant IMU calibration file '")+dataFile+"'");
	}
	DBG_FCT
	try
	{
		ifstream in;
		ostringstream msg;
		in.open(dataFile);
		if (!in.good()) {
			msg.clear();
			msg << "Cannot open file '"<<dataFile <<"'.";
			throw runtime_error(msg.str());
		}
		LOG_IF(DBG, DEBUG) << "Reading calibration file '" << dataFile <<"'....";
		CSV_Row row(in);

		// Accelerometers
		bool result=row.getDoubleArrayFromNextLine(accelOffset);
		if (!result) throw  runtime_error("Missing accel. offset in IMU calibration file");
		result=row.getDoubleArrayFromNextLine(accelResolution);
		if (!result) throw  runtime_error("Missing accel. resolution in IMU calibration file");
		// Magnetic calibration
		result=row.getDoubleArrayFromNextLine(magOffset);
		if (!result) throw  runtime_error("Missing magn. offset in IMU calibration file");
		for (auto i = 0UL; i<3;i++) {
			result=row.getDoubleArrayFromNextLine(magTransformationMatrix[i]);
			if (!result) throw  runtime_error("Incomplete magn. transformation matrix in IMU calibration file");
		}
		// Gyroscopes
		result=row.getDoubleArrayFromNextLine(gyroOffset);
		if (!result) throw  runtime_error("Missing gyro. offset in IMU calibration file");
		result=row.getDoubleArrayFromNextLine(gyroResolution);
		if (!result) throw runtime_error("Missing gyro. resolution in IMU calibration file");
		LOG_IF(DBG, DEBUG) << "Calibration file processed. ";
	}
	catch(exception &e) {
		cout << "Exception while parsing IMU calibration file: '" << dataFile << "'" << endl;
		cout << e.what() << endl;
		throw;
	}
}
