/*
 *  IsaTwoRecord.h
 *
 */

#pragma once

#include <iostream>
#include <array>
#include <chrono>
#include "CSV_Row.h"
#include "CSV_Record.h"
#include "IsaTwoInterface.h"

using namespace std;
using namespace std::chrono;

//define INCLUDE_AHRS_DATA   	// Define to transport AHRS output data.
//define INCLUDE_GPS_VELOCITY	// Define to transport GPS velocity and velocity direction.

/** @ingroup RecordProcessing
 * @brief A DataSet containing all data obtained from the CanSat except images.
 *  This class collects the data, can be initialized by reading from a CSV file,
 *  and can output a CSV representation of the record (1200).
 *  See System Architecture Document for details.
 */
class IsaTwoRecord  : public CSV_Record {
public:
	IsaTwoRecord();
	IsaTwoRecord(const IsaTwoRecord&)=delete;
	virtual ~IsaTwoRecord();

	// Public data members carried by the record
	unsigned long timestamp{}; // in msec.
	// A. IMU data
	array<int16_t, 3> accelRaw{};  		/**< Raw accelerometer readings */
	array<int16_t, 3> gyroRaw{};		/**< Raw gyroscope readings */
	array<double, 3> magRaw{};		/**< Uncalibrated magnetometer readings, in µT */

	// B. GPS data
	bool  GPS_Measures{};			/**< true if GPS data is included in the record */
	double GPS_LatitudeDegrees{};	/**< The latitude in decimal degrees, ([-90;90], + = N, - = S) */
	double GPS_LongitudeDegrees{};   /**< The longitude in decimal degrees ([-180;180], + =E, - =W) */
	double GPS_Altitude{};			/**< Altitude of antenna, in meters above mean sea level (geoid) */
#ifdef INCLUDE_GPS_VELOCITY
	double GPS_VelocityKnots{};		/**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
	double GPS_VelocityAngleDegrees{}; /**< Direction of velocity in decimal degrees, 0 = North */
#endif
#ifdef INCLUDE_AHRS_DATA
	// C. AHRS data
	array<double,3>  AHRS_Accel{};	/**< Acceleration as computed by the AHRS */
	double roll{}, yaw{}, pitch{};  /**< Euler angles as computed by the AHRS */
#endif
	// D. Primary mission data
	double temperatureBMP{};		/**< Temperature in °C obtained from the BMP280 */
    double pressure{};				/**< Pressure in hPa   obtained from the BMP280 */
    double altitude{};				/**< Altitude in m	   obtained from the BMP280 */
    double temperatureThermistor{};	/**< Temperature in °C obtained from the thermistor */
	// E. Secondary mission data
    double CO2{};					/**< eCO2 reading from CCS811 sensor (ppm) */

	/** set all data to 0 or "" */
	virtual void clear();
	/** setReferenceTime to current time */
	virtual void initReferenceTime();
	/** Update timestamp, relative to referenceTime */
	virtual void updateTimeStamp();

	virtual void printCSV_Header(ostream &os) const;
	virtual void printCSV(ostream &os) const;
	/** Obtain the record-type field (should always have the same value for a particular type of record */
	virtual unsigned int getRecordType() const { return recordType; }

protected:
	using CSV_Record::printCSV;
	/** Protected constructor: for use by subclasses that would add additional data members.
	 * @param numExtraElements The number of data elements added by the subclass
	 * @param recordType The record type (to be set in the first field of the record).
	 */
	IsaTwoRecord(unsigned int numExtraElements, unsigned int recordType);

	/** Assign values to the various data members, from the data found in the CSV_Row.
	 *  @exception runtime_error if the recordType is not the expected one.
	 *  @param row The CSV_Row to extract data from.
	 *  @return The index of the next item in the row (if any). This information is used
	 *  by subclasses, which could parse additional fields from the row.
	 */
	virtual unsigned int parseRow(const CSV_Row &row);

	unsigned int recordType;	/**< The type of record (as defined in IsaTwoInterface.h). */
	high_resolution_clock::time_point refTimePoint; /** Reference for timestamping the records */

    friend ostream & operator<<(ostream &os, const IsaTwoRecord & record);
    friend istream& operator>>(istream& is,  IsaTwoRecord & record);
};

ostream & operator<<(ostream &os, const IsaTwoRecord & record);
istream& operator>>(istream& is,  IsaTwoRecord & record);


