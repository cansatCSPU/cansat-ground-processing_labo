/*
 * PrimaryMission.cpp
 *
 *  Created on: 23 mars 2019
 *      Author: Steven et Bryan
 */

#include <PrimaryMission.h>

PrimaryMission::PrimaryMission(const RTP_ConfigDataSet theRTP_Config)
	: RTP_Config(theRTP_Config){}

PrimaryMission::~PrimaryMission() {}

bool PrimaryMission::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut){
	recordOut=recordIn;

	//Temperature calibration
	double resultTemp = recordIn.temperatureThermistor + RTP_Config.temperatureOffset;

	//Pressure calibration
	double resultPres = recordIn.pressure - RTP_Config.PressureOffest;

	//Altitude calibration
	double resultAlt = recordIn.altitude - RTP_Config.localReferentialOrigin_Altitude;

	//Storage
	bool ok = true;
	for(unsigned long i = 0; i < 3; i++){
		ok =ok && storeResult(resultTemp, recordOut.temperatureCorrected,"temp");
		//ok =ok && storeResult(resultPres, recordOut.p,"pres");
		ok =ok && storeResult(resultAlt, recordOut.altitude_Corrected,"alt");
	}
	return ok;
}

