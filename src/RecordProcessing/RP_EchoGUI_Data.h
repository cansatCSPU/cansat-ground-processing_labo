/*
 * RP_EchoGUI_Data.h
 *
 */

#pragma once

#include <RecordProcessor/RecordProcessor.h>
#include "IsaTwoGroundRecord.h"
#include <iostream>

/**  @ingroup RecordProcessing
 *  @brief A processor to echo the provided record on a stream and transparently forward
 *  input data.
 */
class RP_EchoGUI_Data: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	/** Constructor
	 *  @param outputStream The ostream on which record must be echoed
	 */
	RP_EchoGUI_Data(std::ostream &outputStream) : RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>(), os(outputStream) {};

	/** Just echo part of the provided record on the output stream provided in the constructor.
	 *  (only data relevant to the RT GUI.
	 * @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 * @param recordOut The record resulting from the process: it is set to recordIn.
	 * @return Always true.
	 */
	bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
		os << "ts=" << std::setfill('0') << std::setw(10) << recordIn.timestamp
		   << std::fixed << std::setprecision(numDecimals)
		   << ",   X="  <<  std::setw(numDecimals+3) << recordIn.positionLocal[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.positionLocal[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.positionLocal[2]
		   << ", R="  <<  std::setw(numDecimals+3) << recordIn.groundAHRS_Roll
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.groundAHRS_Yaw
		   << ", P="  <<  std::setw(numDecimals+3) << recordIn.groundAHRS_Pitch
		   << std::setprecision(2)
		   << "    t°="   << std::setw(2+3) << recordIn.temperatureBMP << '/' << recordIn.temperatureCorrected << "°C"
		   << ", alt.="   << std::setw(2+5) << recordIn.altitude << " m"
		   << ", press.=" << std::setw(2+5) << recordIn.pressure << " Pa"
		   << std::setprecision(0)
		   << ", CO2=" << recordIn.CO2_Concentration << "ppm" << std::endl;
		recordOut=recordIn;
		return true;};

private:
	std::ostream& os;
	static constexpr unsigned short numDecimals=6;
};

