/*
 * DummyRecordSource.h
 */

#pragma once
#include "RecordSource.h"
#include "IsaTwoRecord.h"

/** @ingroup RecordSource
 *  @brief A RecordSource which generates random IsaTwoRecords, indefinetely.
 *  Operation can be interrupted by pressing return, as for any
 *  processor.
 *  Intended use: use a RP_Delay processor to have the records
 *  processed at a realistic rate (this class justy emits records
 *  as fast as possible).
 *  Used for test only. */
class DummyRecordSource : public RecordSource {
public:
	DummyRecordSource(RecordSource::visualFeedback_t feedback, unsigned long period);
	virtual ~DummyRecordSource();
	/** Feed the processor with one random IsaTwoRecord.
	 *  @param  processor  The processor to feed.
	 *  @param	recordProcessed True if a record was found and processed. In this case, the result of the
	 *  		processing is in parameter success.
	 *  @param  success This parameter is updated to true by the subclass if the processing of
	 *  	            the record was successful, and to false otherwise. The value is irrelevant
	 *  	            if recordProcessed is false.
	 *  @return true if the method should be called again (because there is data left to process)
	 *          false if the last record has been processed or some error make it impossible
	 *          to process additional records.
	 */
	virtual bool feedOneRecordToProcessor(Processor &processor, bool& recordProcessed, bool &success);
protected:
	IsaTwoRecord record;
	unsigned long period; /**< The generation period, in msec. */
};

