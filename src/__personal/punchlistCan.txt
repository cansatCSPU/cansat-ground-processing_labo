Avril 2019: BUGS:
  - RT-Commander: getFile: bloque la canette si gros !
    Problème pour l'instant: tout passe bien sur le sérial, mais on perd un msg sur deux à la radio.
    POURQUOI??? Est-ce le line stream qui pose problème ? Voir avec l'UT si tout est ok.
    OUI Avec l'UT tout va bien.... Afficher ce que reçoit la radio... 
    
  - mman.h n'existe pas sur Windows. Faire une version windows de la fonction copyFile à la fin de FileMgr. 
    FAIT. UT OK sur Windows. 
    A retester MacOS et Win après avoir commité et mis à jour le sous-module. 
    
    
 A NOTER DANS LES PROCEDURES: 
  - Carte 8GB dans le master, sinon long à l'init.
  

BLOCAGE SLAVE: Arrive même avec l'USB branché. 
C'est toujours après:
"Waiting after powerup (50 msec)
Files will be stored in directory: 2Img0204"
Plus précisément: dans le checkCamera (après le 3e éclair rouge, après le double).
Arrive quasiment immédiatement après avoir enlevé la fiche USB avec DBG_BEGIN activé. 
Ca cale aussi parfois avec l'USB, après le message "Checking 0V2640"
								  après le message "Isa2Imager.begin()"
Corrigé un problème d'overflow du compteur de répertoire: après 255 ça boucle indéfiniment.
Le problème reste: bloque après le premier flash vert, donc dans l'accès I2C.
Nettement moins souvent lorsque l'USB est connecté. Cela modifie-t-il le timing ?

Epuiserait-on la mémoire ? Avec la carte SD ? 
Je constate que:
   - mySD.exists crée un File, qui est ouvert.
   - DESTRUCTOR_CLOSES_FILE est à 0 donc ce fichier n'est pas fermé. A tester à 1 ? A l'aveuglette?
   
Faudrait-il des pull-ups sur l'I2C maintenant que la camera est seule ? 
Oui il en faut. Mais ça ne résout rien.

Y aurait-il un problème de vitesse de l'I2C. Arducam assure que 100kHz est OK
La vitesse par défaut de la Feather serait-elle 400kHz ? Non. Checked in 
~/Library/Arduino15/packages/arduino/hardware/samd/1.6.20/libraries/Wire/Wire.cpp:
default clock frequency = 100000 (TWI_CLOCK macro).
Sans surprise, ajouter un Wire.SetClock(100000); explicite ne change rien.

Jouer avec le délai de mise en marche de la caméra ?
Modifié le toggleCtrl pour avoir des cycles 15 sec On/2 sec Off
Augmenté le délai entre l'allumage du régulateur et l'init à 1 seconde.
Pas d'effet.

Tester sans la SD et sans prendre d'image ?
A. Sans prendre d'images dans le cycle: 
Pas d'effet.
B. Sans initialiser la carte SD dans le cycle
Pas d'effet: le problème est donc lié à la caméra SEULE. 

A propos du freeze I2C: visiblement la librairie n'est pas fullproof....
https://forum.arduino.cc/index.php/topic,19624.0.html
Mais ceci concerne la librarie TWI, or la version SAMD utilise les Sercoms. 

Ne pas interrompre le regulateur pour voir ? 
YESSS! 50 minutes sans erreurs!

Les pullups: devraient-il être vers le 3V de la carte ? les deux ? 
Ajouté des pulls-ups entre le I2C et le 3.3V de la slave.
Lancé à 10:53 en rétablissant le régulateur OK 15 min
Rétabli la SD, et la prise d'images.
Lancé à 11:05 répertoires créés àpd de 61


   


* Branch TestSocketServer:
	- Test on MacOS : ok with Processing3
	- Test on Windows: compilation OK. Not tested: cannot ping my VM/
		Corrected network config in VirtualBox. Now responding on 192.168.56.1.
	    Retest with Processing3...
* Armadillo
		3a. Integrate armadillo lib. (required? not urgent!)
* Prj Mngt
			
		3c. Improve documentation about RT-Processing architecture.
		  
		4. Define tasks: 
			A. Transparent processing chain: 
					1. transparent from IsaTwoRecords: (chain-type=transparent
						Delay processor + transparent (IsaTwo -> Ground)
					2. Transparent from IsaTwoGround records:
						Delay processor only. 
			B. IMU Calibration (in 1 processor ?)
			C. AHRS_Madgwick fusion. 
			D. Conversion of GPS position and velocity to cartesian coordinates.
		5. Prepare simulated files for KF ?
	   
	   
Utilisation de FeatherM0 Express (branch Feather Test & Support)
- DebugCSPU.h Test OK. 
- HardwareScanner OK
    ADC steps, defaultReferenceVoltage et RF_Port, OK avec les 2 cartes. 
- Serial2-3 OK 
- EEPROM bank: OK  
- TO DO: vérifier les casts de et vers EEPROM: il faut que ce soit aligné! 
  Donc, caster de record vers void*: pas de souci. l'inverse ne va pas: il faut 
  que le buffer soit déclaré comme un record. 
- SD_Logger à retester.
    OK sur Uno. 
    Tester avec Feather OK. Un
- FlashLogger: Test OK. 
    Perfos largement meilleurs sans fermer le fichier tous les 100 bytes (21 msec/100 bytes vs 200 ms/100byte). 
    Perfos ci-dessus à retester: ce n'est pas ce que fait le programme de test??
    Implémenter un mode sans fermeture à chaque log (conditionnel à LOGGER_CLOSE_AFTER_EACH_ACCESS).
- Accès Flash à tester (récupération des fichiers).  Faut-il un utilitaire pour lister ? 
- Restriction IsaTwoRecord aux données brutes + CO2_Voltage. 
	- codé.OK
	- UT à adapter OK
	- données à valider. OK 
- doc GND: installé les groupes (début), ne fonctionne pas en imbriqué? 
- Wire initialization: où se trouve-t-elle ? A CLARIFIER DANS LE DOC D'ARCHITECTURE!
     - dans SD_Logger ?
     - dans FlashLogger ? 
     - dans les drivers IMU et autres I2C ? 
     - Comment faire pour que ça ne se passe qu'une fois ??
- IsaTwoHW_Scanner à créer
- Test de AcquisitionProcess:
	* heartBeat: éviter de stocker le statut
	* Ajouter pour ça une toggleLED() sur le modèle de setLED()
	* limiter le clignotement des acquisition ? 
	
- GPS: problème d'update à 10Hz: voir https://forums.adafruit.com/viewtopic.php?t=58977

- Communication entre cartes: class ClockSynchronizer_Master et _Slave ok. 
TODO: A documenter dans l'architecture canette. 
-  DINIT_IF_PIN Codé dans branche, à tester/fusionner. Fait, OK. 

Pour accélérer un peu l'acquisition (nécessité à confirmer en fonction du temps SD.
- Revenir à une classe sanns superclasse pour IMU_ClientLSMxxx et NXDxxxx, virer toutes
  les méthodes virtuelles ? 
- Faire un memset dans le clear de l'IsaTwoRecord ?