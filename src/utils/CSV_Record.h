/*
 * CSV_Record.h
 *
 */

#pragma once
#include <iostream>
#include <array>
#include <fstream>
#include <assert.h>
#include "CSV_Row.h"

/*! @ingroup Utilities
 * @brief An abstract base class for any record which can be stored as a CSV line.
 * In order to implement a particular type of record, just
 *   - declare as many public data members as data in the record
 *   - implement the 3 public methods clear(), printCSV() and printCSV_Header()  to clear them,
 *     output them in CSV format and output the file header.
 *   - implement virtual method parseRow() to populate them from the a row.
 *
 *  To read from a file:
 *  @code
 *  CSV_RecordSubclass rec;
 *  rec.openInputFileName("xxx");
 *  while (rec.read()) {
 *     if (rec.readOK()) {
 *     		// do something with record
 *     }
 *  } // while
 *  rec.closeFiles();
 *  @endcode
 *
 *  To write to file:
 *  @code
 *  CSV_RecordSubclass rec;
 *  rec.openOutputFileName("xxx");
 *  ... repeat as often as required ....
 *  	populate the data members
 *  	rec.write;
 *  rec.closeFiles();
 *  @endcode
 *
 *  Read and write operations can be combined:
 *  @code
 *  CSV_RecordSubclass rec;
 *  rec.openInputFileName("xxx");
 *  rec.openOutputFileName("yyyy");
 *  while (rec.read()) {
 *     if (rec.readOK()) {
 *     		// modify the record
 *     		rec.write();
 *     }
 *  }// while
 *  rec.closeFiles();
 *  @endcode
 *
 */
class CSV_Record {
public:
	static constexpr auto separator=", ";
	static constexpr auto numFloatDecimalPositions=5;
	/** Constructor
	 *  @param numDataElements The number of data elements in the record. If non 0, the number of elements
	 *  will be checked again this number for each record read from file. If 0, the number of elements will be
	 *  retrieved from the first non-comment line and all subsequent records will be checked against it.
	 */
	CSV_Record(unsigned int numDataElements) : _numData(numDataElements), _readOK(false) { };
	virtual ~CSV_Record();
	CSV_Record(const CSV_Record& other) = delete;
	/** Copy constructor.
	 * @param other The record to copy from.
	 */
	CSV_Record & operator=(const CSV_Record& other);

	/** Initialise the record with a string containing a valid CSV_Representation of a single
	 *  record (witout a final "\\n").
	 *  This method must not be overridden by subclasses.
	 *  @param recordAsCSV_String The string to parse to initialise the record.
	 */
	void loadFromCSV_String(const std::string& recordAsCSV_String);

	/** @name Methods to be overridden */
	/** @{ */
	/** set all data to 0 or "" */
	virtual void clear()=0;
	/** Output the record in CSV format on the stream (no final carriage-return)
	 *  @param os The stream to write to.
	 */
	virtual void printCSV(std::ostream &os) const = 0;
	/** Output the record header in CSV format on the stream  (no final carriage-return)
	 *  @param os The stream to write to.
	 */
	virtual void printCSV_Header(std::ostream &os) const = 0;
	/** @} End methods to be overridden */

	/** @name Public methods to read/write from/to CSV file */
	/** @{ */
	/** @brief Open the input file.
	 *  @param filePath Full or relative path to the file to read from.
	 *  @param skipHeader If true, the header line is skipped.
	 *  @throw runtime_error if the file does not exist, or is not readable.
	 */
	virtual void openInputFile(const std::string &filePath, bool skipHeader=true);

	/* @brief Open the output file.
	 * If the file exists, it is silently overwritten. The header is immediately written.
	 * The file is kept open and ready for further calls to write();
	 * @param filePath Full- or relative path to the file to write to.
	 * @param printHeader If true the header line is input to the file.
	 */
	virtual void openOutputFile(const std::string &filePath, bool printHeader=true);

	/** Detect whether the end of the input file has been reached
	 * @return true if end of file is reached, false otherwise
	 */
	virtual bool eof() { assert(ifs); return !ifs->good(); };
	/** Write the record to the currently open output file */
	virtual void write();
	/** Read and parse a line from the current input file
	 *  @return True is the end of file is reached, false otherwise. */
	virtual bool read();
	/** Close input and output files (if any). */
	virtual void closeFiles();
	/** @}  group of methods to read/write files */

	// Public methods to use to read from stream
	virtual void readFromCSV(std::istream &is);
	/** Check whether the last read operation was successful. Always check before
	 *  using a record read from file or from stream.
	 *  @return true if ok, false otherwise.
	 */
	virtual bool readOK() { return _readOK; };

protected:
	/** @name Protected methods to be overridden */
	/** @{ */
	/** Parse the data from the row.
	 *  @param row The row to parse
	 *  @return The index of the next item in the row (if any). This information is used
	 *  by subclasses, which could parse additional fields from the row.
	 */
	virtual unsigned int parseRow(const CSV_Row &row)=0;
	/** @} End of methods to be overridden */

	/** Print an std::array of floats in CSV format, on the provided stream.
	 *  @param os The destination stream
	 *  @param arr The array to output
	 *  @param asInt If true, float values are converted into integers.
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os, const std::array<float, 3> &arr,
									bool asInt, bool finalSeparator) const;

	/** Print an std::array of double in CSV format, on the provided stream.
	 *  @param os The destination stream
	 *  @param arr The array to output
	 *  @param asInt If true, float values are converted into integers.
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os, const std::array<double, 3> &arr,
										bool asInt, bool finalSeparator) const;

	/** Print an std::array of double in CSV format, on the provided stream.
	 *  @param os The destination stream
	 *  @param arr The array to output
	 *  @param asInt If true, float values are converted into integers.
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os, const std::array<double, 4> &arr,
										bool asInt, bool finalSeparator) const;

	/** Print an std::array of uint16_t in CSV format, on the provided stream.
	 *  @param os The destination stream
	 *  @param arr The array to output
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os, const std::array<uint16_t, 3> &arr,
									bool finalSeparator) const;

	/** Print an std::array of uint16_t in CSV format, on the provided stream.
	 *  @param os The destination stream
	 *  @param arr The array to output
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os, const std::array<int16_t, 3> &arr,
									bool finalSeparator) const;

	/** Print a float with the appropriate number of decimal positions, on the provided stream.
	 *  @param os The destination stream
	 *  @param f The float to output
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os,
							const float f,
							bool finalSeparator) const;

	/** Print a double with the appropriate number of decimal positions, on the provided stream.
	 *  @param os The destination stream
	 *  @param d The double to output
	 *  @param finalSeparator If true, a final comma is added.
	 */
	virtual void printCSV(	std::ostream &os,
							const double d,
							bool finalSeparator) const;

	/** Read an array of 3 floats from the row, starting at index. Index is incremented by 3
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param arr	The array to fill with the values from the row.
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, std::array<float,3> &arr) const;
	/** Read an array of 3 doubles from the row, starting at index. Index is incremented by 3
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param arr	The array to fill with the values from the row.
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, std::array<double,3> &arr) const;
	/** Read an array of 4 doubles from the row, starting at index. Index is incremented by 4
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param arr	The array to fill with the values from the row.
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, std::array<double,4> &arr) const;
	/** Read an array of 3 uint16_t from the row, starting at index. Index is incremented by 3
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param arr	The array to fill with the values from the row.
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, std::array<uint16_t,3> &arr) const;
	/** Read an array of 3 int16_t from the row, starting at index. Index is incremented by 3
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param arr	The array to fill with the values from the row.
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, std::array<int16_t,3> &arr) const;
	/** Read a float from the row, starting at index. Index is incremented by 1
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param f		The float to set to the value obtained from the row
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, float& f)  const;
	/** Read a double from the row, starting at index. Index is incremented by 1
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param d		The double to set to the value obtained from the row
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, double& d)  const;

	/** Read a boolean from the row, starting at index. Index is incremented by 1
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param b		The bool to set to the value obtained from the row
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, bool& b)  const ;
	/** Read an unsigned long from the row, starting at index. Index is incremented by 1
	 * @param row 	The row to read from
	 * @param index The index within the row from which to read (it is incremented after successful read)
	 * @param n		The unsigned long to set to the value obtained from the row
	 */
	virtual void readFromRow(const CSV_Row &row, unsigned int &index, unsigned long& n)  const ;

private:
	unsigned long _numData; /*!< the number of data elements in the line storing the record */
	bool _readOK; /*!< True if the record has been successfully read from file */
	std::ifstream *ifs{};
	std::ofstream *ofs{};

	friend std::ostream & operator<<(std::ostream &os, const CSV_Record & record);
	friend std::istream& operator>>(std::istream& is,  CSV_Record & record);
};

std::ostream & operator<<(std::ostream &os, const CSV_Record & record);
std::istream& operator>>(std::istream& is,  CSV_Record & record);
