/*
 * CalculationUtils.cpp
 *
 *  A couple of useful utility functions
 *  TODO: This function should be generalized and included in the appropriate class.
 */

#include "CalculationUtils.h"
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

void printDifferences(const array<float, 3> &a, const array<float,3> &b) {
	float diff[3], percent[3];
	string warning[3];
	for (unsigned int i = 0; i<3; i++ )  {
		diff[i]=fabs(a[i]-b[i]);
		percent[i]=diff[i]/a[i]*100.0f;
		if (fabs(percent[i]) > 5) warning[i]= "*** ERROR ? ***";
	}
	cout << fixed << '['
		 << setprecision(5) << diff[0] << " (" << setprecision(1) << percent[0] << "%" << warning[0] << "),"
		 << setprecision(5) << diff[1] << " (" << setprecision(1) << percent[1] << "%" << warning[1] << "),"
		 << setprecision(5) << diff[2] << " (" << setprecision(1) << percent[2] << "%" << warning[2] << ")]";
}


