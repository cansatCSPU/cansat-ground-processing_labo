/*
 * DebugUtils.h
 *
 * The header file to include to benefit from all debugging/tracing/logging features provided in
 * utils and easylogging++.
 */

#pragma once

#include "DbgFunction.h"
#include "easylogging++.h"

// Configure logging defaults:
void configureLogging();




