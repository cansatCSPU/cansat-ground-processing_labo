/*
 * MinGW_ThreadProxy.h
 *
 * This file defines whether the MinGW complements to the thread library
 * must be included of not (MinGW has an incomplete implementation of
 * the thread standard library, while MinGW-W64 is complete).
 */

#pragma once

#ifdef __MINGW32__
#include <stdlib.h> // inclusion required to have __MINGW64_VERSION_MAJOR defined
					// by MinGW-W64
#   ifndef __MINGW64_VERSION_MAJOR
	// Note __MINGWW32__ is defined by MinGW-W64 and __MING64__ is not defined
	//      by MinGW-W64-32bits.
	// MinGW (not MinGW-W64) has an incomplete implementation of C++11 thread. complement it with
	// by including mingw.thread.h. Unfortunately this file brings some problems:

	  // 1. Declare function GetNativeSystemInfo, which seems to be missing on W10.
	  // and is required by the mingw.thread.h
#     include <windows.h>
		void GetNativeSystemInfo(LPSYSTEM_INFO lpSystemInfo);

		// 2. Silence conversion warnings
#       pragma GCC diagnostic push
#       pragma GCC diagnostic ignored "-Wconversion"
#    	include "mingw.thread.h"
#       pragma GCC diagnostic pop
#	endif
#endif
