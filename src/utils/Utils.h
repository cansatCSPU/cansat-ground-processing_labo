/*
 * Utils.h
 *
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the package should include a tag @ingroup Utils
 * in the class documentation block.
 */

 /** @defgroup Utilities Utilities
 *  @brief The set of classes providing general purpose utilities.
 *
 *  The Utilities package contains a collections classes providing generic (mostly) cross-platform
 *  utilities for debugging, file system operation, stream manipulation, CSV format parsing etc.
 *
 *  _Dependencies_\n
 *  This package relies on:
 *  	- the easylogging++ library
 *
 *  @todo //.
 *
 *
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (IsaTwo).
 */





