#include "TestRecordProcessor.h"
#include "cute.h"

#include "RecordProcessor/StringProcessor.h"
#include "RecordProcessor/RecordProcessor.h"
#include "RecordProcessor/RP_Echo.h"
#include "RecordProcessor/RP_RecordDelay.h"
#include "RP_Example.h"
#include "IsaTwoRecord.h"
#include "DebugUtils.h"
//constexpr auto DBG=false;

#include <chrono>

using namespace std;


// For test: just append "Dummy" after the string and echo on cout.
class Processor_EchoAddDummy : public StringProcessor {
	public:
	Processor_EchoAddDummy() : StringProcessor() {};
	bool doProcess(const std::string& recordIn, std::string& recordOut) {
		recordOut=recordIn+"Dummy";
		cout <<"Dummy added: "<< recordOut << std::endl;
		return true;
	};
};

// For test: just increase timestamp by 1.
class Processor_IncreaseTS : public RecordProcessor<IsaTwoRecord, IsaTwoRecord> {
	public:
	Processor_IncreaseTS() : RecordProcessor<IsaTwoRecord, IsaTwoRecord>() {};

	virtual bool doProcess(const IsaTwoRecord& recordIn, IsaTwoRecord& recordOut) {
		recordOut = recordIn;
		ASSERT_EQUAL(recordIn.timestamp, recordOut.timestamp);
		recordOut.timestamp++;
		cout << "recordOut.Timestamp is now increased: " << recordOut.timestamp << endl;
		return true;
	};
};

void Test_StringBasedProcessor() {
	// NB: this test just cannot fail...
	RP_StringEcho echo(cout);
	ASSERT_EQUAL(nullptr, echo.getFinalResultRecord());
	string strIn="This is the input string";
	echo.processString(strIn);
	ASSERT_EQUAL(echo.getFinalResultRecord(), nullptr);
	ASSERT_EQUAL(strIn, echo.getFinalResult());
	// String should be echoed.
	RP_RecordEcho<IsaTwoRecord> echoRecord(cout);
	IsaTwoRecord record;
	echoRecord.processRecord(record);
	// record should be echoed in CSV format.
}

void Test_StringBasedFollowedByStringBased(){
	Processor_EchoAddDummy dummyProcessor, dummyProcessor2, dummyProcessor3;
	RP_StringEcho echo(cout), echo2(cout), echo3(cout), echo4(cout);
	// Attempt to append echo to itself
	ASSERT_THROWS(echo.appendProcessor(echo),runtime_error);
	echo.appendProcessor(dummyProcessor);
	// Attempt to append the same processor twice
	ASSERT_THROWS(echo.appendProcessor(dummyProcessor),runtime_error);
	dummyProcessor3.appendProcessor(echo4);
	// here we have: echo -> dummy and dummy3 -> echo4
	// Let's attempt to make a cycle echo4->dummy3 ->echo4
	ASSERT_THROWS(echo4.appendProcessor(dummyProcessor3),runtime_error);

	cout << "Testing results with 3 processors" << endl;
	echo.appendProcessor(echo2);
	echo.processString("Test");
	// echo -> dummy -> echo2. This should output "Test"
	ASSERT_EQUAL(echo.getFinalResultRecord(), nullptr);
	ASSERT_EQUAL(echo.getLocalResultRecord(), nullptr);
	ASSERT_EQUAL("Test",echo.getLocalResult() );
	ASSERT_EQUAL("TestDummy",echo.getFinalResult() );

	cout << "Testing results with 4 processors" << endl;
	// echo -> dummy -> echo -> dummy  This should output "Test\nTestDummy"
	echo.appendProcessor(dummyProcessor2);
	echo.processString("Test");
	ASSERT_EQUAL(echo.getFinalResultRecord(), nullptr);
	ASSERT_EQUAL("TestDummyDummy",echo.getFinalResult() );

	cout << "Testing results with 5 processors" << endl;
	echo.appendProcessor(echo3);
	// echo -> dummy -> echo -> dummy -> echo
	// This should output "Test\nTestDummy\nTestDummyDummy"
	echo.processString("Test");
	ASSERT_EQUAL(echo.getFinalResultRecord(), nullptr);
	ASSERT_EQUAL("TestDummyDummy",echo.getFinalResult() );

	// The same appending processors differently
	cout << "Testing results with 5 processors (bis)" << endl;
	Processor_EchoAddDummy dummyBis, dummyBis2;
	RP_StringEcho echoBis(cout), echoBis2(cout), echoBis3(cout);
	dummyBis2.appendProcessor(echoBis3);
	echoBis2.appendProcessor(dummyBis2);
	dummyBis.appendProcessor(echoBis2);
	echoBis.appendProcessor(dummyBis);
	echoBis.processString("TestBis");
	// we should have: echoBis->dummyBis->echoBis2->dummyBis2->echoBis3
	ASSERT_EQUAL("TestBisDummyDummy",echoBis.getFinalResult() );
}

void TestCSV_Record_Processor(){
	IsaTwoRecord in;
	const IsaTwoRecord *out;
	Processor_IncreaseTS  inc, inc2;
	RP_RecordEcho<IsaTwoRecord> echo1(cout);
	in.timestamp=999L;
	inc.processRecord(in);
	out=dynamic_cast<const IsaTwoRecord*>(inc.getFinalResultRecord());
	ASSERT_EQUAL(1000L, out->timestamp);

	string expectedAfterTS=", 0, 0, 0, " // type, timestamp, accel
			"0, 0, 0, 0.00000, 0.00000, 0.00000, " //gyo + mag
			"0, 0.00000, 0.00000, 0.00000, "; // GPS
#ifdef INCLUDE_GPS_VELOCITY
	expectedAfterTS +="0.00000, 0.00000, ";
#endif
#ifdef INCLUDE_AHRS_DATA
	expectedAfterTS +="0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, ";
#endif
	expectedAfterTS +="0.00000, 0.00000, 0.00000, 0.00000, " // primary
			   "0.00000"; // secundary: CO2_Voltage
	string expected="0, 1000"+expectedAfterTS;
	ASSERT_EQUAL(expected, inc.getFinalResult());

	in.timestamp=999L;
	inc.appendProcessor(inc2);
	inc.processRecord(in);
	out=dynamic_cast<const IsaTwoRecord*>(inc.getFinalResultRecord());
	ASSERT_EQUAL(1001L, out->timestamp);
	expected="0, 1001"+expectedAfterTS;
	ASSERT_EQUAL(expected, inc.getFinalResult());
	out=inc.getSpecializedResultRecord();
	ASSERT_EQUAL(1000L, out->timestamp);
	expected="0, 1000"+expectedAfterTS;
	ASSERT_EQUAL(expected, inc.getLocalResult());


	// Test that csv->echo + getCSV_Result -> null.
	inc.appendProcessor(echo1);
	inc.processRecord(in);
	expected="0, 1001"+expectedAfterTS;
	ASSERT_EQUAL(expected, inc.getFinalResult());
	ASSERT_EQUAL(1001, dynamic_cast<const IsaTwoRecord*>(inc.getFinalResultRecord())->timestamp);
}

void test_IsaTwoRecordDelay(){
	RP_RecordDelay<IsaTwoRecord> proc;
	IsaTwoRecord  rec;
	rec.timestamp= 43265;
    auto endLast=std::chrono::high_resolution_clock::now();

	for (unsigned long delay = 0; delay< 1000;delay+=100) {
		rec.timestamp+=delay;
		proc.processRecord(rec);
		auto end=std::chrono::high_resolution_clock::now();

		milliseconds measuredDelay= duration_cast<std::chrono::milliseconds>(end-endLast);
		milliseconds delayError=measuredDelay-std::chrono::milliseconds(delay);

		// Lt's no print anything, unless there is an error (this disrupts the timing,
		// especially on virtual boxes.
		if (delayError.count() > 10)
		{
			cout << "Delay=" << delay << " milliseconds" << endl;
			cout << "  Measured delay: " << std::chrono::milliseconds(measuredDelay).count() <<" msec" << endl;
			cout << "  Error on delay: " << std::chrono::milliseconds(delayError).count() << " msec" << endl;
			ASSERT(delayError.count() <= 10);
		}
		endLast=std::chrono::high_resolution_clock::now();
	}
}


void TestCombinations_csvCsvCsvString(){
	Processor_IncreaseTS  inc1, inc2, inc3;
	Processor_EchoAddDummy dummy;
	inc1.appendProcessor(inc2);
	inc1.appendProcessor(inc3);
	IsaTwoRecord in;
	const IsaTwoRecord *out;
	in.timestamp=999L;
	// Should provide a string as result.
	// last processor converts CSV into string.
	inc1.processRecord(in);
	out=dynamic_cast<const IsaTwoRecord*>(inc1.getFinalResultRecord());
	ASSERT(out!=nullptr);
	ASSERT_EQUAL(1002, out->timestamp);

	cout << "Appending dummy string processor" << endl;
	inc1.appendProcessor(dummy);
#ifdef RECORD_PROCESS_DBG_UTILITIES
	cout << inc1.getProcessorChainDescription();
#endif
	inc1.processRecord(in);
	out=dynamic_cast<const IsaTwoRecord*>(inc1.getFinalResultRecord());
	ASSERT_EQUAL(nullptr, out);
	string expectedAfterTS=", 0, 0, 0, " // type, timestamp, accel
			"0, 0, 0, 0.00000, 0.00000, 0.00000, " //gyo + mag
			"0, 0.00000, 0.00000, 0.00000, "; // GPS pos+ altitude
#ifdef INCLUDE_GPS_VELOCITY
	expectedAfterTS+="0.00000, 0.00000, "; // GPS velocity
#endif
#ifdef INCLUDE_AHRS_DATA
	expectedAfterTS +="0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, ";
#endif
	expectedAfterTS +="0.00000, 0.00000, 0.00000, 0.00000, " // primary
			   "0.00000"; // secundary: CO2_Voltage
	string expected="0, 1002"+expectedAfterTS+"Dummy";
	ASSERT_EQUAL(expected, inc1.getFinalResult());
}

void TestCombination_csvStringcsv(){
	// Should provide a CSV as result: the last processor reparses the string
	// into an input CSV.
	Processor_IncreaseTS  inc1, inc2;
	RP_StringEcho echo(cout);
	inc1.appendProcessor(echo);
	inc1.appendProcessor(inc2);
	IsaTwoRecord in;
	in.timestamp = 30;
	inc1.processRecord(in);
	const IsaTwoRecord *out=inc2.getSpecializedResultRecord();
	ASSERT(out!=nullptr);
	ASSERT_EQUAL(32, out->timestamp);
}

void TestResultValidation() {
	IsaTwoGroundRecord in, out;
	RP_Example proc;
	proc.activateResultsValidation(true);

	in.timestamp = 1000;
	in.groundAHRS_Roll=500.0;
	ASSERT(proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    	dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
		500.0, 1E-8);

    in.timestamp = 1000;
	in.groundAHRS_Roll=500.00001;
	ASSERT(!proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    	dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
		500.0, 1E-8);

    in.timestamp = 999;
	in.groundAHRS_Roll=499.5000;
	ASSERT(proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    	dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
		499.5, 1E-8);

    // Ditto, no validation
    proc.activateResultsValidation(false);

    in.timestamp = 1000;
    in.groundAHRS_Roll=500.0;
    ASSERT(proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    		dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
			500.0, 1E-8);

    in.timestamp = 1000;
    in.groundAHRS_Roll=500.00001;
    ASSERT(proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    		dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
			500.0, 1E-8);

    in.timestamp = 999;
    in.groundAHRS_Roll=499.5000;
    ASSERT(proc.processRecord(in));
    ASSERT_EQUAL_DELTA(
    		dynamic_cast<const IsaTwoGroundRecord*>(proc.getFinalResultRecord())->groundAHRS_Roll,
			499.5, 1E-8);
}

cute::suite make_suite_TestRecordProcessor() {
	cute::suite s { };
	s.push_back(CUTE(Test_StringBasedProcessor));
	s.push_back(CUTE(Test_StringBasedFollowedByStringBased));
	s.push_back(CUTE(TestCSV_Record_Processor));
	s.push_back(CUTE(TestCombinations_csvCsvCsvString));
	s.push_back(CUTE(TestCombination_csvStringcsv));
	s.push_back(CUTE(test_IsaTwoRecordDelay));
	s.push_back(CUTE(TestResultValidation));

	return s;
}
